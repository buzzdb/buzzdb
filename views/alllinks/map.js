function(doc) {
  if (doc.type == 'link') {
    emit(doc.title, doc);
  }
};