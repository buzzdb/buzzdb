function(doc) {
  if (doc.type == 'comment') {
    emit(doc.path[0], doc);
  }
};